import { Component, OnInit } from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {MatDialog} from '@angular/material/dialog';
import {DeactivateDialogComponent} from '../dialogs/deactivate-dialog/deactivate-dialog.component';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss']
})
export class ProfileViewComponent implements OnInit {

  avatar = 'assets/imgs/avatar.png';
  isSuperAdmin = false;
  selectedAction = 'users';

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, public dialog: MatDialog) {
    iconRegistry.addSvgIcon(
    'user-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/user-profile.svg'));

    iconRegistry.addSvgIcon(
      'pending-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/pending-profile.svg'));

    iconRegistry.addSvgIcon(
      'deactivate-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/deactivate-profile.svg'));
  }

  ngOnInit(): void {
  }

  deactivate(): void{
    const dialogRef = this.dialog.open(DeactivateDialogComponent, {
      height: '200px',
      width: '500px',
    });
  }

  selectOption(option: string): void{
    this.selectedAction = option;
  }
}
