import {Component, Inject, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


export interface DeactivateDialogData {
  id: string;



}

@Component({
  selector: 'app-deactivate-dialog',
  templateUrl: './deactivate-dialog.component.html',
  styleUrls: ['./deactivate-dialog.component.scss']
})
export class DeactivateDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeactivateDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DeactivateDialogData) { }

  ngOnInit(): void {


  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
