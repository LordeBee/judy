import {Component, Inject, OnInit, Output, EventEmitter} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StorageServiceService} from '../../../services/storage-service.service';
import {ToastService} from '../../../services/toast.service';
import {ProfileServiceService} from '../../../services/profile-service.service';
import {hostHeaderInfo} from '../../../account/models/hostHeaderInfo';
import {UtilService} from '../../../services/util.service';


export interface InviteDialogData {
  name: string;
  role: string;


}

@Component({
  selector: 'app-invite-dialog',
  templateUrl: './invite-dialog.component.html',
  styleUrls: ['./invite-dialog.component.scss']
})
export class InviteDialogComponent implements OnInit {

  loader = false;
  formBody: any;
  public hostHeaderInfo: hostHeaderInfo;
  getRoleObj: any;
  userData: any;
  rolesResp: any;
  roles: Array<any>;
  formGroup: FormGroup;
  @Output() onSubmit = new EventEmitter();
  authResp: any;

  constructor(public dialogRef: MatDialogRef<InviteDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: InviteDialogData,
              private formBuilder: FormBuilder, public utilService: UtilService,
              // tslint:disable-next-line:max-line-length
              public apiService: ProfileServiceService, public storageService: StorageServiceService, private toastService: ToastService) {
    this.hostHeaderInfo = new hostHeaderInfo();
    if (!this.getRoleObj){
      this.getRoleObj = {
        hostHeaderInfo: '',
        searchType: 0,
        id: ''
      };
    }


    if (!this.formBody){
      this.formBody = {
        hostHeaderInfo: this.hostHeaderInfo,
        id: 1,
        companyId: '',
        fName: '',
        oname: '',
        lname: '',
        email: '',
        contact: '',
        role: ''
      };
    }


    this.formGroup = this.formBuilder.group({
      fName: ['', Validators.required],
      oname: [''],
      lname: ['', Validators.required],
      email: ['', Validators.required],
      contact: ['', Validators.required],
      role: ['', Validators.required]
    });

    this.roles = [];

  }

  ngOnInit(): void {


    this.userData = JSON.parse(this.storageService.getData());

    this.getCompanyRole();

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  done(): void{
    if (this.formGroup.valid){
      this.formBody.companyId = this.userData.companyInfo.id;
      this.formBody.fName = this.formGroup.get('fName').value;
      this.formBody.oname = this.formGroup.get('oname').value;
      this.formBody.lname = this.formGroup.get('lname').value;
      this.formBody.email = this.formGroup.get('email').value;
      this.formBody.contact = this.formGroup.get('contact').value;
      this.formBody.role = this.formGroup.get('role').value;
      this.loader = true;

      this.apiService.inviteUser(this.formBody).subscribe( res => {
        this.authResp = res;
        if (this.authResp.hostHeaderInfo.responseCode === '000'){
          this.utilService.showToast('User invited successfully');
          this.onSubmit.emit('000');
          this.onNoClick();
        }else{
          this.loader = false;
          this.utilService.showToast(this.authResp.hostHeaderInfo.responseMessage);
        }
      }, error => {
        this.loader = false;
        this.utilService.showToast('Please try again later');

      });

    }else{
      this.utilService.markFormGroupTouched(this.formGroup);
    }

  }

  getCompanyRole(): void{
    this.getRoleObj.id = this.userData.companyInfo.id;
    this.getRoleObj.hostHeaderInfo = this.hostHeaderInfo;
    this.apiService.getUserRoles(this.getRoleObj).subscribe(res => {
      this.rolesResp = res;
      if (this.rolesResp.hostHeaderInfo.responseCode === '000'){
        this.roles = this.rolesResp.roles;
      }
    });
  }

}
