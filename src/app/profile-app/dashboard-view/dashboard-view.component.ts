import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {RoleDialogComponent} from '../dialogs/role-dialog/role-dialog.component';
import {InviteDialogComponent} from '../dialogs/invite-dialog/invite-dialog.component';
import {Router} from '@angular/router';

export interface DashboardData {
  id: any;
  firstName: any;
  otherName: any;
  lastName: any;
  email: any;
  mobileNumber: any;
  address: any;
  role: any;
}

const ELEMENT_DATA: DashboardData[] = [
  {
    id: '0986210',
    firstName: 'Hannah',
    otherName: 'Christine',
    lastName: 'Baers',
    email: 'hBaers@outlook.com',
    mobileNumber: '020 876 3212',
    address: 'Lake Shore, Tulsa',
    role: 'Role 1'
  },
  {
    id: '0986210',
    firstName: 'Hannah',
    otherName: 'Christine',
    lastName: 'Baers',
    email: 'hBaers@outlook.com',
    mobileNumber: '020 876 3212',
    address: 'Lake Shore, Tulsa',
    role: 'Role 1'
  },
  {
    id: '0986210',
    firstName: 'Hannah',
    otherName: 'Christine',
    lastName: 'Baers',
    email: 'hBaers@outlook.com',
    mobileNumber: '020 876 3212',
    address: 'Lake Shore, Tulsa',
    role: 'Role 1'
  }
];

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.scss']
})

export class DashboardViewComponent implements OnInit {

  selectedAction = 'users';
  pageWidth: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['select', 'id', 'firstName', 'otherName', 'lastName', 'email', 'mobileNumber', 'address', 'role'];
  dataSource = new MatTableDataSource<DashboardData>(ELEMENT_DATA);
  selection = new SelectionModel<DashboardData>(true, []);
  isSuperAdmin = false;

  displayedColumnsMobile: string[] = ['id'];

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, public dialog: MatDialog, private router: Router) {

    this.pageWidth = window.innerWidth;

    iconRegistry.addSvgIcon(
      'user-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/user-profile.svg'));

    iconRegistry.addSvgIcon(
      'pending-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/pending-profile.svg'));

    iconRegistry.addSvgIcon(
      'add-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/add-profile.svg'));

    iconRegistry.addSvgIcon(
      'search-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/search-profile.svg'));

    iconRegistry.addSvgIcon(
      'admin-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/admin-profile.svg'));

    iconRegistry.addSvgIcon(
      'mine-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mine-profile.svg'));


  }

  ngOnInit(): void {
    this.pageWidth = window.innerWidth;
    setTimeout(() => this.dataSource.paginator = this.paginator);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.pageWidth  = event.target.innerWidth;
  }

  selectOption(option: string): void{
    this.selectedAction = option;
  }


  isAllSelected(): any {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }


  masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  logSelection(): void {
    // this.selection.selected.forEach(s => console.log(s.name));
  }

  applyFilter(event: Event): any {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openRoleDialog(): void{
    const dialogRef = this.dialog.open(RoleDialogComponent, {
      height: '340px',
      width: '500px',
    });
  }

  inviteUser(): void{
    const dialogRef = this.dialog.open(InviteDialogComponent, {
      height: '630px',
      width: '500px',
    });
  }

  view(): void{
    this.router.navigate(['/profile/profile-view']);
  }


  getFirstLetter(str: string): string{
    const first = str.substring(0, 1);
    return first;
  }

  closeNav(): void{
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("overlay").style.display = "none";


  }

  openNav(): void {
    document.getElementById("mySidenav").style.width = "280px";
    document.getElementById("overlay").style.display = "block";



  }

}
