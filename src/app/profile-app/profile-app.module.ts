import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileAppRoutingModule } from './profile-app-routing.module';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import {SharedModule} from '../shared/shared.module';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import { RoleDialogComponent } from './dialogs/role-dialog/role-dialog.component';
import { InviteDialogComponent } from './dialogs/invite-dialog/invite-dialog.component';
import { ProfileViewComponent } from './profile-view/profile-view.component';
import { DeactivateDialogComponent } from './dialogs/deactivate-dialog/deactivate-dialog.component';


@NgModule({
  declarations: [DashboardViewComponent, RoleDialogComponent, InviteDialogComponent, ProfileViewComponent, DeactivateDialogComponent],
  entryComponents: [RoleDialogComponent, InviteDialogComponent, DeactivateDialogComponent],
  imports: [
    CommonModule,
    ProfileAppRoutingModule,
    SharedModule,
    MatTableModule,
    MatPaginatorModule
  ]
})
export class ProfileAppModule { }
