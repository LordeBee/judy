import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
declare var $: any;
import {ExportDialogComponent} from '../dialogs/export-dialog/export-dialog.component';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import * as Highcharts from 'highcharts';

export interface DashboardData {
  type: any;
  cases: any;
  associates: any;

}

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.scss']
})
export class DashboardViewComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumnsMobile: string[] = ['type'];
  dataSource: any;

  activity = 'default';
  pageWidth: any;

  highcharts = Highcharts;
  chartOptions = {
    chart: {
      type: 'spline',
      style: {
        fontFamily: 'Muli-Regular'
      }
    },
    plotOptions: {
      series: {
        marker: {
          enabled: false
        }
      }
    },
    title: {
      text: ''
    },
    subtitle: {
      text: ''
    },
    xAxis: {
      lineColor: 'transparent',
      lineWidth: 1,
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      opposite: true
    },
    yAxis: {
      gridLineDashStyle: 'longdash',
      lineColor: '#ccc',
      lineWidth: 1,
      title: {
        text: ''
      },
      labels: {
        enabled: false
      }
    },
    tooltip: {
      enabled: false
    },
    series: [
      {
        color: '#ccc',
        name: 'Tokyo',
        data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
      },
      {
        color: '#707070',
        name: 'New York',
        data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
      }

    ],
    legend: {
      enabled: false
    },
  };


  options = [
    {type: 'Crimmigration Consultation', cases: '30', associates: '7'},
    {type: 'Criminal Defence', cases: '14', associates: '14'},
    {type: 'Business Management', cases: '14', associates: '14'},
    {type: 'Estate Planning', cases: '14', associates: '60'},
    {type: 'Personal Injury', cases: '14', associates: '60'},
    {type: 'Litigation', cases: '14', associates: '14'},
    {type: 'Crimmigration Consultation', cases: '30', associates: '7'},
    {type: 'Criminal Defence', cases: '14', associates: '14'},
    {type: 'Business Management', cases: '14', associates: '14'},
    {type: 'Estate Planning', cases: '14', associates: '60'},
    {type: 'Personal Injury', cases: '14', associates: '60'},
    {type: 'Litigation', cases: '14', associates: '14'}
  ];

  selectedOption = 1;
  filter = '';
  sampleText = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.';

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, public dialog: MatDialog, private router: Router) {

    this.pageWidth = window.innerWidth;

    iconRegistry.addSvgIcon(
      'mine-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mine-profile.svg'));

    iconRegistry.addSvgIcon(
      'new-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/new-practice.svg'));

    iconRegistry.addSvgIcon(
      'edit-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/edit-practice.svg'));

    iconRegistry.addSvgIcon(
      'export-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/export-practice.svg'));

    iconRegistry.addSvgIcon(
      'search-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/search-profile.svg'));

    iconRegistry.addSvgIcon(
      'crimmigration-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/crimmigration-practice.svg'));

    iconRegistry.addSvgIcon(
      'criminal-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/criminal-practice.svg'));

    iconRegistry.addSvgIcon(
      'business-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/business-practice.svg'));

    iconRegistry.addSvgIcon(
      'estate-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/estate-practice.svg'));

    iconRegistry.addSvgIcon(
      'personal-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/personal-practice.svg'));

    iconRegistry.addSvgIcon(
      'litigation-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/litigation-practice.svg'));

  }

  ngOnInit(): void {
  this.dataSource = new MatTableDataSource<DashboardData>(this.options);
    setTimeout(() => {
      $('.dropify').dropify({
        tpl: {
          message: '<div class="dropify-message"><p class="preview-message">Drag and drop supporting documents</p></div>',
        }
      });
    }, 10);


  }

  add(): void{
    if (this.activity !== 'new'){
      this.activity = 'new';
      setTimeout(() => {
        $('.dropify').dropify({
          tpl: {
            message: '<div class="dropify-message"><p class="preview-message">Drag and drop supporting documents</p></div>',
          }
        });
      }, 10);
      this.selectedOption = 0;
      this.options.unshift( {type: 'Practice Name', cases: 'Number of', associates: 'Number of'});
    }
    else{
      this.options.shift();
      this.selectedOption = 1;
      this.activity = 'default';
    }
  }

  export(): void{
    this.dialog.open(ExportDialogComponent, {
      height: '300px',
        width: '400px',
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.pageWidth  = event.target.innerWidth;
  }

  applyFilter(event: Event): any {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getWidth(): boolean{
    setTimeout(() => this.dataSource.paginator = this.paginator);
    let status = false;
    status = this.pageWidth <= 1024 && this.pageWidth > 600;
    return status;
  }

  goToView(): void{
    this.router.navigate(['/practice/practice-view']);
  }


}


