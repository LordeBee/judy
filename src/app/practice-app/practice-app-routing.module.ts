import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardViewComponent} from './dashboard-view/dashboard-view.component';
import { PracticeViewComponent } from './practice-view/practice-view.component';

const routes: Routes = [
  {path: 'dashboard', component: DashboardViewComponent},
  {path: 'practice-view', component: PracticeViewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PracticeAppRoutingModule { }
