import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PracticeAppRoutingModule } from './practice-app-routing.module';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import {SharedModule} from '../shared/shared.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {FormsModule} from '@angular/forms';

import { ExportDialogComponent } from './dialogs/export-dialog/export-dialog.component';
import { PracticeViewComponent } from './practice-view/practice-view.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import { HighchartsChartModule } from 'highcharts-angular';




@NgModule({
  declarations: [DashboardViewComponent, ExportDialogComponent, PracticeViewComponent],
  entryComponents: [ExportDialogComponent],
  imports: [
    CommonModule,
    PracticeAppRoutingModule,
    SharedModule,
    Ng2SearchPipeModule,
    FormsModule,
    MatPaginatorModule,
    MatTableModule,
    HighchartsChartModule
  ]
})
export class PracticeAppModule { }
