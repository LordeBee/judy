import { Component, OnInit } from '@angular/core';
import {ExportDialogComponent} from '../dialogs/export-dialog/export-dialog.component';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {MatDialog} from '@angular/material/dialog';
declare var $: any;
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-practice-view',
  templateUrl: './practice-view.component.html',
  styleUrls: ['./practice-view.component.scss']
})
export class PracticeViewComponent implements OnInit {


  highcharts = Highcharts;
  chartOptions = {

    chart: {
      type: 'spline',
      style: {
        fontFamily: 'Muli-Regular'
      }
    },
    plotOptions: {
      series: {
        marker: {
          enabled: false
        }
      }
    },
    title: {
      text: ''
    },
    subtitle: {
      text: ''
    },
    xAxis: {
      lineColor: 'transparent',
      lineWidth: 1,
      categories: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      opposite: true,
      labels: {
        style: {
          color: '#D1D1D1',
          align: 'cenetr',
          fontSize: '7px',
        }
      }
    },
    yAxis: {
      gridLineDashStyle: 'longdash',
      lineColor: '#ccc',
      lineWidth: 1,
      title: {
        text: ''
      },
      labels: {
        enabled: false
      }
    },
    tooltip: {
      enabled: false
    },
    series: [
      {
        color: '#ccc',
        name: 'Tokyo',
        data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
      },
      {
        color: '#707070',
        name: 'New York',
        data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
      }

    ],
    legend: {
      enabled: false
    },
  };



  activity = 'default';
  pageWidth: any;


  options = [
    {type: 'Crimmigration Consultation', cases: '30', associates: '7'},
    {type: 'Criminal Defence', cases: '14', associates: '14'},
    {type: 'Business Management', cases: '14', associates: '14'},
    {type: 'Estate Planning', cases: '14', associates: '60'},
    {type: 'Personal Injury', cases: '14', associates: '60'},
    {type: 'Litigation', cases: '14', associates: '14'},
    {type: 'Crimmigration Consultation', cases: '30', associates: '7'},
    {type: 'Criminal Defence', cases: '14', associates: '14'},
    {type: 'Business Management', cases: '14', associates: '14'},
    {type: 'Estate Planning', cases: '14', associates: '60'},
    {type: 'Personal Injury', cases: '14', associates: '60'},
    {type: 'Litigation', cases: '14', associates: '14'}
  ];

  selectedOption = 1;
  filter = '';
  sampleText = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.';
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, public dialog: MatDialog) {

    iconRegistry.addSvgIcon(
      'new-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/new-practice.svg'));

    iconRegistry.addSvgIcon(
      'edit-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/edit-practice.svg'));

    iconRegistry.addSvgIcon(
      'export-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/export-practice.svg'));
  }

  ngOnInit(): void {
  }


  add(): void{
    if (this.activity !== 'new'){
      this.activity = 'new';
      setTimeout(() => {
        $('.dropify').dropify({
          tpl: {
            message: '<div class="dropify-message"><p class="preview-message">Drag and drop supporting documents</p></div>',
          }
        });
      }, 5);
      this.selectedOption = 0;
      this.options.unshift( {type: 'Practice Name', cases: 'Number of', associates: 'Number of'});
    }else{
      this.activity = 'default';
    }
  }

  export(): void{
    this.dialog.open(ExportDialogComponent, {
      height: '300px',
      width: '400px',
    });
  }

}
