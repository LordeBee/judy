import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AccountComponent} from './account/account/account.component';
import {MainComponent} from './layouts/main/main/main.component';


const routes: Routes = [

  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      }
    ]
  },

  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'profile',
        loadChildren: () => import('./profile-app/profile-app.module').then(m => m.ProfileAppModule)
      }
    ]
  },

  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'practice',
        loadChildren: () => import('./practice-app/practice-app.module').then(m => m.PracticeAppModule)
      }
    ]
  },

  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'master',
        loadChildren: () => import('./master-app/master-app.module').then(m => m.MasterAppModule)
      }
    ]
  },


  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'invoice',
        loadChildren: () => import('./invoice-app/invoice-app.module').then(m => m.InvoiceAppModule)
      }
    ]
  },

  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'expense',
        loadChildren: () => import('./expense-app/expense-app.module').then(m => m.ExpenseAppModule)
      }
    ]
  },

  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'case',
        loadChildren: () => import('./case-app/case-app.module').then(m => m.CaseAppModule)
      }
    ]
  },

  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'file',
        loadChildren: () => import('./file-app/file-app.module').then(m => m.FileAppModule)
      }
    ]
  },

  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'document',
        loadChildren: () => import('./document-app/document-app.module').then(m => m.DocumentAppModule)
      }
    ]
  },

  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'calender',
        loadChildren: () => import('./calender-app/calender-app.module').then(m => m.CalenderAppModule)
      }
    ]
  },

  {
    path: '',
    component: AccountComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./account/account.module').then(m => m.AccountModule)
      }
    ]
  },

  { path: '**', redirectTo: '/login', pathMatch: 'full' }

  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
