import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CaseAppRoutingModule } from './case-app-routing.module';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import { ExportDialogComponent } from './dialogs/export-dialog/export-dialog.component';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [DashboardViewComponent, ExportDialogComponent],
    imports: [
        CommonModule,
        CaseAppRoutingModule,
        SharedModule
    ]
})
export class CaseAppModule { }
