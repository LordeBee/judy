import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {SelectionModel} from '@angular/cdk/collections';
import {ExportDialogComponent} from '../dialogs/export-dialog/export-dialog.component';
declare var $: any;
import {hostHeaderInfo} from '../../models/hostHeaderInfo';
import {StorageServiceService} from '../../services/storage-service.service';
import {ToastService} from '../../services/toast.service';
import {CaseAppServiceService} from '../../services/case-app-service.service';

export interface DashboardData {
  title: any;
  desc: any;

}

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.scss']
})
export class DashboardViewComponent implements OnInit {

  selection = new SelectionModel<DashboardData>(true, []);

  activity = 'default';
  pageWidth: any;
  public hostHeaderInfo: hostHeaderInfo;

  options = [
    {title: 'Civil Case', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    {title: 'Civil Case', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    {title: 'Civil Case', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    {title: 'Civil Case', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    {title: 'Civil Case', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    {title: 'Civil Case', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    {title: 'Civil Case', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    {title: 'Civil Case', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    {title: 'Civil Case', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    {title: 'Civil Case', desc: 'Lorem ipsum dolor sit amet, consectetuer '},

  ];

  selectedOption = 0;
  userData: any;
  getRolesObj: any;
  isData = false;
  loader = true;
  response: any;
  sampleText = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.';

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, public dialog: MatDialog, private router: Router,
              public apiService: CaseAppServiceService, public storageService: StorageServiceService, private toastService: ToastService) {
    this.pageWidth = window.innerWidth;
    this.hostHeaderInfo = new hostHeaderInfo();

    if (!this.getRolesObj) {
      this.getRolesObj = {
        hostHeaderInfo: this.hostHeaderInfo,
        companyID: ''
      };
    }

    iconRegistry.addSvgIcon(
      'mine-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mine-profile.svg'));

    iconRegistry.addSvgIcon(
      'new-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/new-practice.svg'));

    iconRegistry.addSvgIcon(
      'edit-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/edit-practice.svg'));

    iconRegistry.addSvgIcon(
      'export-practice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/export-practice.svg'));

    iconRegistry.addSvgIcon(
      'search-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/search-profile.svg'));
  }

  ngOnInit(): void {

    this.userData = JSON.parse(this.storageService.getData());
    // this.getRolesObj.companyID = this.userData.companyInfo.id;
    this.getRolesObj.companyID = 2;
    this.getCompanyCases();

    setTimeout(() => {
      $('.dropify').dropify({
        tpl: {
          message: '<div class="dropify-message"><p class="preview-message">Drag and drop supporting documents</p></div>',
        }
      });
    }, 10);




  }

  add(): void{

    if (this.activity !== 'new'){
      this.activity = 'new';
      setTimeout(() => {
        $('.dropify').dropify({
          tpl: {
            message: '<div class="dropify-message"><p class="preview-message">Drag and drop supporting documents</p></div>',
          }
        });
      }, 10);
      this.selectedOption = 0;
      this.options.unshift( {title: 'Case Name', desc: 'Summary'});
    }
    else{
      this.options.shift();
      this.selectedOption = 1;
      this.activity = 'default';
    }

  }

  export(): void{

    this.dialog.open(ExportDialogComponent, {
      height: '300px',
      width: '400px',
    });

  }


  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.pageWidth  = event.target.innerWidth;
  }

  isAllSelected(): any {
    const numSelected = this.selection.selected.length;
    const numRows = this.options.length;
    return numSelected === numRows;
  }


  masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.options.forEach(row => this.selection.select(row));
  }

  getCompanyCases(): void{
    this.apiService.getCompanyCaseType(this.getRolesObj).subscribe(res => {
      console.log(res)
      this.response = res;

    });
  }
}

