import {Component, HostListener, OnInit} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog} from '@angular/material/dialog';
import {ExportDialogComponent} from '../dialogs/export-dialog/export-dialog.component';
import {InvoiceDialogComponent} from '../dialogs/invoice-dialog/invoice-dialog.component';
import {NewInvoiceDialogComponent} from '../dialogs/new-invoice-dialog/new-invoice-dialog.component';
import * as Highcharts from 'highcharts';
// import the module
// import * as Variablepie from 'highcharts/modules/variable-pie';
// Variablepie(Highcharts);
require('highcharts/modules/variable-pie')(Highcharts);


export interface DashboardData {
  id: any;
  dateAdded: any;
  name: any;
  value: any;

}

const ELEMENT_DATA: DashboardData[] = [
  {
    id: '128957',
    dateAdded: '14/03/2020',
    name: 'Shrubs & Co Law Firm',
    value: '$1,000'
  },
  {
    id: '128957',
    dateAdded: '14/03/2020',
    name: 'Shrubs & Co Law Firm',
    value: '$1,000'
  },
  {
    id: '128957',
    dateAdded: '14/03/2020',
    name: 'Shrubs & Co Law Firm',
    value: '$1,000'
  },
  {
    id: '128957',
    dateAdded: '14/03/2020',
    name: 'Shrubs & Co Law Firm',
    value: '$1,000'
  },
  {
    id: '128957',
    dateAdded: '14/03/2020',
    name: 'Shrubs & Co Law Firm',
    value: '$1,000'
  },
  {
    id: '128957',
    dateAdded: '14/03/2020',
    name: 'Shrubs & Co Law Firm',
    value: '$1,000'
  },
  {
    id: '128957',
    dateAdded: '14/03/2020',
    name: 'Shrubs & Co Law Firm',
    value: '$1,000'
  },
  {
    id: '128957',
    dateAdded: '14/03/2020',
    name: 'Shrubs & Co Law Firm',
    value: '$1,000'
  },
  {
    id: '128957',
    dateAdded: '14/03/2020',
    name: 'Shrubs & Co Law Firm',
    value: '$1,000'
  }
];

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.scss']
})
export class DashboardViewComponent implements OnInit {

  displayedColumns: string[] = ['select', 'id', 'dateAdded', 'name', 'value', 'edit'];
  dataSource = new MatTableDataSource<DashboardData>(ELEMENT_DATA);
  selection = new SelectionModel<DashboardData>(true, []);




  highcharts = Highcharts;

  chartOptions = {
    chart: {
      type: 'spline',
      style: {
        fontFamily: 'Muli-Regular'
      }
    },
    plotOptions: {
      series: {
        marker: {
          enabled: false
        }
      }
    },
    title: {
      text: ''
    },
    subtitle: {
      text: ''
    },
    xAxis: {
      lineColor: 'transparent',
      lineWidth: 1,
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    },
    yAxis: {
      lineColor: '#ccc',
      lineWidth: 1,
      title: {
        text: ''
      },
      labels: {
        enabled: true
      }
    },
    tooltip: {
      enabled: false
    },
    series: [
      {
        color: '#6C0E23',
        name: 'Tokyo',
        data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
      },
      {
        color: '#00AD6C',
        name: 'New York',
        data: [0, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
      }

    ],
    legend: {
      enabled: false
    },
  };

  chartOptionsPie = {
    chart: {
      type: 'variablepie',
      width: 300
    },
    title: {
      text: ''
    },
    credits: { enabled: false },
    tooltip: { enabled: false },
    plotOptions: {
      variablepie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false,
        }
      }
    },
    series: [{
      minPointSize: 10,
      innerSize: '20%',
      zMin: 0,
      name: 'countries',
      data: [{
        name: 'Lorem ipsum dolor sit',
        color: '#930808',
        y: 505370,
        z: 92.9
      }, {
        name: 'Lorem ipsum dolor sit',
        color: '#CE6464',
        y: 551500,
        z: 118.7
      }, {
        name: 'Lorem ipsum dolor sit',
        color: '#0C866A',
        y: 312685,
        z: 124.6
      }, {
        name: 'Lorem ipsum dolor sit',
        color: '#E8A539',
        y: 301340,
        z: 201.8
      }, {
        name: 'Lorem ipsum dolor sit',
        color: '#3D497E',
        y: 41277,
        z: 214.5
      }, {
        name: 'Lorem ipsum dolor sit',
        color: '#391313',
        y: 357022,
        z: 235.6
      }]
    }]
  };

  pageWidth: any;
  chartDivLayout = 'row';
  chartDivAlign = "center center";
  chartsDivLayout = 'row';

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, public dialog: MatDialog) {

    iconRegistry.addSvgIcon(
      'mine-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mine-profile.svg'));

    iconRegistry.addSvgIcon(
      'search-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/search-profile.svg'));

    iconRegistry.addSvgIcon(
      'new-invoice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/new-invoice.svg'));

    iconRegistry.addSvgIcon(
      'export-invoice',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/export-invoice.svg'));
  }

  ngOnInit(): void {
    this.pageWidth = window.innerWidth;
    if(this.pageWidth < 1025){
      this.chartDivLayout = 'column';
      this.chartDivAlign = "start";
    }else{
      this.chartDivLayout = 'row';
      this.chartDivAlign = "center center";
    }

    if(this.pageWidth < 600){
      this.chartsDivLayout = 'column';
    }else{
      this.chartsDivLayout = 'row';

    }


  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.pageWidth  = event.target.innerWidth;
    if(this.pageWidth < 1025){
      this.chartDivLayout = 'column';
      this.chartDivAlign = "start";
    }else{
      this.chartDivLayout = 'row';
      this.chartDivAlign = "center center";
    }


    if(this.pageWidth < 600){
      this.chartsDivLayout = 'column';
    }else{
      this.chartsDivLayout = 'row';

    }
  }

  applyFilter(event: Event): any {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  isAllSelected(): any {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }


  masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  export(): void{
    this.dialog.open(ExportDialogComponent, {
      height: '300px',
      width: '400px',
    });
  }

  view(): void{
    this.dialog.open(InvoiceDialogComponent,{
      height: '730px',
      width: '700px',
    });
  }

  new(): void{
    this.dialog.open(NewInvoiceDialogComponent,{
      height: '680px',
      width: '750px',
    });
  }


}
