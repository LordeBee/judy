import {Component, Inject, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';



export interface invoiceDialogData {
  name: string;
  role: string;


}

@Component({
  selector: 'app-invoice-dialog',
  templateUrl: './invoice-dialog.component.html',
  styleUrls: ['./invoice-dialog.component.scss']
})
export class InvoiceDialogComponent implements OnInit {

  items = [
    {desc: 'Lorem ipsum dolor sit amet, consectetuer ' , price: '$40 / hr', totalTime: '4 hrs', total: '$80 '},
    {desc: 'Lorem ipsum dolor sit amet, consectetuer ' , price: '$40 / hr', totalTime: '4 hrs', total: '$80 '},
    {desc: 'Lorem ipsum dolor sit amet, consectetuer ' , price: '$40 / hr', totalTime: '4 hrs', total: '$80 '},
    {desc: 'Lorem ipsum dolor sit amet, consectetuer ' , price: '$40 / hr', totalTime: '4 hrs', total: '$80 '},
    {desc: 'Lorem ipsum dolor sit amet, consectetuer ' , price: '$40 / hr', totalTime: '4 hrs', total: '$80 '},
    {desc: 'Lorem ipsum dolor sit amet, consectetuer ' , price: '$40 / hr', totalTime: '4 hrs', total: '$80 '},
    {desc: 'Lorem ipsum dolor sit amet, consectetuer ' , price: '$40 / hr', totalTime: '4 hrs', total: '$80 '}
  ];

  constructor(public dialogRef: MatDialogRef<InvoiceDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: invoiceDialogData) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
