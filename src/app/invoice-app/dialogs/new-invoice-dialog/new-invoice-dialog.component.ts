import {Component, Inject, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';



export interface invoiceDialogData {
  name: string;
  role: string;


}


@Component({
  selector: 'app-new-invoice-dialog',
  templateUrl: './new-invoice-dialog.component.html',
  styleUrls: ['./new-invoice-dialog.component.scss']
})
export class NewInvoiceDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<NewInvoiceDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: invoiceDialogData, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {

    iconRegistry.addSvgIcon(
      'add-dialog',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/add-dialog-invoice.svg'));
  }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
