import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoiceAppRoutingModule } from './invoice-app-routing.module';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import {SharedModule} from '../shared/shared.module';
import {MatTableModule} from '@angular/material/table';
import { ExportDialogComponent } from './dialogs/export-dialog/export-dialog.component';
import { InvoiceDialogComponent } from './dialogs/invoice-dialog/invoice-dialog.component';
import { NewInvoiceDialogComponent } from './dialogs/new-invoice-dialog/new-invoice-dialog.component';
import { HighchartsChartModule } from 'highcharts-angular';



@NgModule({
  declarations: [DashboardViewComponent, ExportDialogComponent, InvoiceDialogComponent, NewInvoiceDialogComponent],
  entryComponents: [ExportDialogComponent, InvoiceDialogComponent, NewInvoiceDialogComponent],
  imports: [
    CommonModule,
    InvoiceAppRoutingModule,
    SharedModule,
    MatTableModule,
    HighchartsChartModule
  ]
})
export class InvoiceAppModule { }
