import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';

import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';

export interface DashboardData {
  id: any;
  firstName: any;
  otherName: any;
  lastName: any;
  email: any;
  mobileNumber: any;
  address: any;
  role: any;
}

const ELEMENT_DATA: DashboardData[] = [
  {
    id: '0986210',
    firstName: 'Hannah',
    otherName: 'Christine',
    lastName: 'Baers',
    email: 'hBaers@outlook.com',
    mobileNumber: '020 876 3212',
    address: 'Lake Shore, Tulsa',
    role: 'Role 1'
  },
  {
    id: '0986210',
    firstName: 'Hannah',
    otherName: 'Christine',
    lastName: 'Baers',
    email: 'hBaers@outlook.com',
    mobileNumber: '020 876 3212',
    address: 'Lake Shore, Tulsa',
    role: 'Role 1'
  },
  {
    id: '0986210',
    firstName: 'Hannah',
    otherName: 'Christine',
    lastName: 'Baers',
    email: 'hBaers@outlook.com',
    mobileNumber: '020 876 3212',
    address: 'Lake Shore, Tulsa',
    role: 'Role 1'
  }
];

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  selectedAction = 'users';
  pageWidth: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['select', 'id', 'firstName', 'otherName', 'lastName', 'email', 'mobileNumber', 'address', 'role'];
  dataSource = new MatTableDataSource<DashboardData>(ELEMENT_DATA);
  selection = new SelectionModel<DashboardData>(true, []);
  isSuperAdmin = false;

  displayedColumnsMobile: string[] = ['id'];
  menuOpen: boolean;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    this.pageWidth = window.innerWidth;

    iconRegistry.addSvgIcon(
      'search-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/search-profile.svg'));

    iconRegistry.addSvgIcon(
      'mine-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mine-profile.svg'));

    iconRegistry.addSvgIcon(
      'files-file',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/files-file.svg'));

    iconRegistry.addSvgIcon(
      'case-file',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/case-file.svg'));

    iconRegistry.addSvgIcon(
      'expense-file',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/expense-file.svg'));

    iconRegistry.addSvgIcon(
      'individual-file',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/individual-file.svg'));

    iconRegistry.addSvgIcon(
      'invoice-file',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/invoice-file.svg'));

    iconRegistry.addSvgIcon(
      'shared-file',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/shared-file.svg'));

    iconRegistry.addSvgIcon(
      'export-file',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/export-file.svg'));

    iconRegistry.addSvgIcon(
      'upload-file',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/upload-file.svg'));
  }

  ngOnInit(): void {
    this.pageWidth = window.innerWidth;
    setTimeout(() => this.dataSource.paginator = this.paginator);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.pageWidth  = event.target.innerWidth;
  }

  selectOption(option: string): void{
    this.selectedAction = option;
  }


  isAllSelected(): any {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }


  masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  logSelection(): void {
    // this.selection.selected.forEach(s => console.log(s.name));
  }

  applyFilter(event: Event): any {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
