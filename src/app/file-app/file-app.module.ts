import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FileAppRoutingModule } from './file-app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import {FlexModule} from '@angular/flex-layout';
import {MatPaginatorModule} from '@angular/material/paginator';
import {SharedModule} from '../shared/shared.module';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTableModule} from '@angular/material/table';


@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    FileAppRoutingModule,
    FlexModule,
    MatPaginatorModule,
    SharedModule,
    MatExpansionModule,
    MatTableModule
  ]
})
export class FileAppModule { }
