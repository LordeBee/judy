import {Component, Inject, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';




export interface expenseDialogData {
  name: string;
  role: string;


}


@Component({
  selector: 'app-new-expense-dialog',
  templateUrl: './new-expense-dialog.component.html',
  styleUrls: ['./new-expense-dialog.component.scss']
})
export class NewExpenseDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<NewExpenseDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: expenseDialogData, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {

    iconRegistry.addSvgIcon(
      'add-dialog',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/add-dialog-invoice.svg'));
  }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
