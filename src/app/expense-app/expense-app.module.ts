import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExpenseAppRoutingModule } from './expense-app-routing.module';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import { ExportDialogComponent } from './dialogs/export-dialog/export-dialog.component';
import { ExpenseDialogComponent } from './dialogs/expense-dialog/expense-dialog.component';
import { NewExpenseDialogComponent } from './dialogs/new-expense-dialog/new-expense-dialog.component';
import {SharedModule} from '../shared/shared.module';
import { HighchartsChartModule } from 'highcharts-angular';
import {MatTableModule} from '@angular/material/table';

@NgModule({
  declarations: [DashboardViewComponent, ExportDialogComponent, ExpenseDialogComponent, NewExpenseDialogComponent],
  entryComponents: [ExportDialogComponent, ExpenseDialogComponent, NewExpenseDialogComponent ],
  imports: [
    CommonModule,
    ExpenseAppRoutingModule,
    SharedModule,
    MatTableModule,
    HighchartsChartModule

  ]
})
export class ExpenseAppModule { }
