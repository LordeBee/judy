import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerationDialogComponent } from './generation-dialog.component';

describe('GenerationDialogComponent', () => {
  let component: GenerationDialogComponent;
  let fixture: ComponentFixture<GenerationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
