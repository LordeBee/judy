import {Component, Inject, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


export interface GenerateDialogData {
  name: string;
  role: string;


}

@Component({
  selector: 'app-generation-dialog',
  templateUrl: './generation-dialog.component.html',
  styleUrls: ['./generation-dialog.component.scss']
})
export class GenerationDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<GenerationDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: GenerateDialogData) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
