import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DocumentAppRoutingModule } from './document-app-routing.module';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import { GenerationDialogComponent } from './dialogs/generation-dialog/generation-dialog.component';
import {SharedModule} from '../shared/shared.module';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';


@NgModule({
  declarations: [DashboardViewComponent, GenerationDialogComponent],
  entryComponents: [GenerationDialogComponent],
  imports: [
    CommonModule,
    DocumentAppRoutingModule,
    SharedModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule
  ]
})
export class DocumentAppModule { }
