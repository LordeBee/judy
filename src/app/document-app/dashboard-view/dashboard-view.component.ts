import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';

import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {GenerationDialogComponent} from '../dialogs/generation-dialog/generation-dialog.component';

export interface DashboardData {
  nameOfDocument: any;
  dateModified: any;
  modifiedBy: any;
  fileSize: any;
  sharing: any;
  other: any;
}

const ELEMENT_DATA: DashboardData[] = [
  {
    nameOfDocument: 'Lorem ipsum dolor sit amet, consectetuer adipis ',
    dateModified: '23/04/2020',
    modifiedBy: 'Christine Hannah Baers',
    fileSize: '50 MB',
    sharing: 'Private',
    other: '6 MB'
  },
  {
    nameOfDocument: 'Lorem ipsum dolor sit amet, consectetuer adipis ',
    dateModified: '23/04/2020',
    modifiedBy: 'Christine Hannah Baers',
    fileSize: '50 MB',
    sharing: 'Private',
    other: '6 MB'
  },
  {
    nameOfDocument: 'Lorem ipsum dolor sit amet, consectetuer adipis ',
    dateModified: '23/04/2020',
    modifiedBy: 'Christine Hannah Baers',
    fileSize: '50 MB',
    sharing: 'Private',
    other: '6 MB'
  },
  {
    nameOfDocument: 'Lorem ipsum dolor sit amet, consectetuer adipis ',
    dateModified: '23/04/2020',
    modifiedBy: 'Christine Hannah Baers',
    fileSize: '50 MB',
    sharing: 'Private',
    other: '6 MB'
  },
  {
    nameOfDocument: 'Lorem ipsum dolor sit amet, consectetuer adipis ',
    dateModified: '23/04/2020',
    modifiedBy: 'Christine Hannah Baers',
    fileSize: '50 MB',
    sharing: 'Private',
    other: '6 MB'
  },
  {
    nameOfDocument: 'Lorem ipsum dolor sit amet, consectetuer adipis ',
    dateModified: '23/04/2020',
    modifiedBy: 'Christine Hannah Baers',
    fileSize: '50 MB',
    sharing: 'Private',
    other: '6 MB'
  },
  {
    nameOfDocument: 'Lorem ipsum dolor sit amet, consectetuer adipis ',
    dateModified: '23/04/2020',
    modifiedBy: 'Christine Hannah Baers',
    fileSize: '50 MB',
    sharing: 'Private',
    other: '6 MB'
  }
];

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.scss']
})
export class DashboardViewComponent implements OnInit {

  selectedAction = 'users';
  pageWidth: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['select', 'nameOfDocument', 'dateModified', 'modifiedBy', 'fileSize', 'other'];
  dataSource = new MatTableDataSource<DashboardData>(ELEMENT_DATA);
  selection = new SelectionModel<DashboardData>(true, []);
  isSuperAdmin = false;

  displayedColumnsMobile: string[] = ['id'];
  menuOpen: boolean;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, public dialog: MatDialog) {
    this.pageWidth = window.innerWidth;

    iconRegistry.addSvgIcon(
      'search-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/search-profile.svg'));

    iconRegistry.addSvgIcon(
      'mine-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mine-profile.svg'));

    iconRegistry.addSvgIcon(
      'files-file',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/files-file.svg'));

    iconRegistry.addSvgIcon(
      'case-file',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/case-file.svg'));

    iconRegistry.addSvgIcon(
      'expense-file',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/expense-file.svg'));

    iconRegistry.addSvgIcon(
      'individual-file',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/individual-file.svg'));

    iconRegistry.addSvgIcon(
      'invoice-file',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/invoice-file.svg'));

    iconRegistry.addSvgIcon(
      'shared-file',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/shared-file.svg'));

    iconRegistry.addSvgIcon(
      'sync-document',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/sync-document.svg'));


  }

  ngOnInit(): void {
    this.pageWidth = window.innerWidth;
    setTimeout(() => this.dataSource.paginator = this.paginator);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.pageWidth  = event.target.innerWidth;
  }

  selectOption(option: string): void{
    this.selectedAction = option;
  }


  isAllSelected(): any {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }


  masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  logSelection(): void {
    // this.selection.selected.forEach(s => console.log(s.name));
  }

  applyFilter(event: Event): any {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  sync(): void{
    this.dialog.open(GenerationDialogComponent,{
      width: '500px',
      height: '380px'
    });
  }
}
