import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StorageServiceService} from './storage-service.service';

@Injectable({
  providedIn: 'root'
})
export class CaseAppServiceService {

  token: any;
  baseUrl = environment.baseUrl;
  getCompanyCaseTypeUrl = `${this.baseUrl}api/v1/case/getCompanyCaseType`;


  private httpHeaders  = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')
    .set('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT')
    .set(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );

  constructor(private http: HttpClient, private storageServiceService: StorageServiceService) { }

  private requestHeaders = {
    headers: this.httpHeaders
  };

  getUserToken(): void{
    this.token = this.storageServiceService.getToken();
  }

  getCompanyCaseType(data: any): any{
    this.getUserToken();
    const info = JSON.stringify(data);
    this.requestHeaders.headers = this.requestHeaders.headers.set('Authorization', `Judy ${this.token}`);
    return this.http.post(this.getCompanyCaseTypeUrl, info, this.requestHeaders);
  }
}
