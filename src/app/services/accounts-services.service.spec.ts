import { TestBed } from '@angular/core/testing';

import { AccountsServicesService } from './accounts-services.service';

describe('AccountsServicesService', () => {
  let service: AccountsServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccountsServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
