import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StorageServiceService} from './storage-service.service';


@Injectable({
  providedIn: 'root'
})
export class AccountsServicesService {


  token: any;
  baseUrl = environment.baseUrl;
  authenticateUrl = `${this.baseUrl}api/v1/login`;
  resetPasswordUrl = `${this.baseUrl}api/v1/resetPassword?email=`;
  changePasswordUrl = `${this.baseUrl}api/v1/changeUserPassword`;
  getUserResetInfoUrl = `${this.baseUrl}api/v1/getUserPasswordResetInfo?uuid=`;
  completeSignUpUrl  = `${this.baseUrl}api/v1/completeSignUp`;

  private httpHeaders  = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')
    .set('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT')
    .set(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );

  constructor(private http: HttpClient, private storageServiceService: StorageServiceService) { }

  private requestHeaders = {
    headers: this.httpHeaders
  };

  getUserToken(): void{
    this.token = this.storageServiceService.getToken();
  }

  authenticateUser(data: any): any {
    return this.http.post(this.authenticateUrl, data, this.requestHeaders);
  }

  resetPassword(data: any): any {
    return this.http.get(this.resetPasswordUrl + data, this.requestHeaders);
  }

  changePassword(data: any): any {
    return this.http.post(this.changePasswordUrl, data, this.requestHeaders);
  }

  getUserResetInfo(data: any): any {
    return this.http.get(this.getUserResetInfoUrl + data, this.requestHeaders);
  }

  completeSignUp(data: any): any {
    return this.http.post(this.completeSignUpUrl, data, this.requestHeaders);
  }





}
