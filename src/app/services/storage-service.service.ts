import { Injectable } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class StorageServiceService {

  constructor(private cookieService: CookieService) { }


  saveToken(token: any): void{
    sessionStorage.setItem('token', token);
  }

  getToken(): string{
    return sessionStorage.getItem('token');
  }


  saveUserData(data: any): void{
    const userData =  JSON.stringify(data);
    sessionStorage.setItem('userData', userData);
  }


  getData(): any{
    const userData = sessionStorage.getItem('userData');
    return JSON.parse(userData);
  }
}
