import { Injectable } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ToastService} from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor(public toastService: ToastService) { }


   markFormGroupTouched(formGroup: FormGroup): any {
    (<any> Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  showToast(message: any): void {
    this.toastService.show({
      text: message,
      type: 'success',
    });
  }
}
