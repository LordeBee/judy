import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StorageServiceService} from './storage-service.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileServiceService {

  token: any;
  baseUrl = environment.baseUrl;
  getUserRolesUrl = `${this.baseUrl}api/v1/roles/all`;
  inviteUserUrl = `${this.baseUrl}api/v1/inviteUser`;

  private httpHeaders  = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')
    .set('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT')
    .set(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );

  constructor(private http: HttpClient, private storageServiceService: StorageServiceService) { }

  private requestHeaders = {
    headers: this.httpHeaders
  };

  getUserToken(): void{
    this.token = this.storageServiceService.getToken();
  }

  getUserRoles(data: any): any{
    this.getUserToken();
    const info = JSON.stringify(data);
    this.requestHeaders.headers = this.requestHeaders.headers.set('Authorization', `Judy ${this.token}`);
    return this.http.post(this.getUserRolesUrl, info, this.requestHeaders);
  }

  inviteUser(data: any): any{
    this.getUserToken();
    const info = JSON.stringify(data);
    this.requestHeaders.headers = this.requestHeaders.headers.set('Authorization', `Judy ${this.token}`);
    return this.http.post(this.inviteUserUrl, info, this.requestHeaders);
  }
}
