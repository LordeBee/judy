import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  monthDays: Array<any>;
  month: any;
  year: any;
  pageWidth: any;


  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    this.monthDays = [];
    this.pageWidth = window.innerWidth;

    iconRegistry.addSvgIcon(
      'mine-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mine-profile.svg'));

    iconRegistry.addSvgIcon(
      'search-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/search-profile.svg'));

    iconRegistry.addSvgIcon(
      'calender-icon',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/calender-icon.svg'));
  }

  ngOnInit(): void {
    const datess = new Date();
    this.month = datess.getMonth();
    this.year = datess.getFullYear();

    this.getDayNames();



  }

  getDays(): any{
    const data = new Date();
    const d = new Date(this.year, this.month + 1, 0);
    return d.getDate();
  }

  getDayNames(): any{

    const drawCalendar = (month, year) => {
      // tslint:disable-next-line:one-variable-per-declaration
      const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        noDays = new Date(year, month + 1, 0).getDate(),
        firstDay = new Date(year, month, 1).getDay();
      return [...new Array(firstDay).fill(''), ...Array.from({length: noDays}, (_, i) => i + 1)]
        .reduce((acc, cur, i) => {
          if (i % 7 === 0) acc.unshift({});
          acc[0][days[i % 7]] = cur;
          return acc;
        }, []).reverse();
    };

    this.monthDays = drawCalendar(this.month, this.year);



  }

  getMonthName(index: any): any{

    const month = [];
    month[0] = 'January';
    month[1] = 'February';
    month[2] = 'March';
    month[3] = 'April';
    month[4] = 'May';
    month[5] = 'June';
    month[6] = 'July';
    month[7] = 'August';
    month[8] = 'September';
    month[9] = 'October';
    month[10] = 'November';
    month[11] = 'December';
    return month[index];
  }

  next():void{
    if (this.month === 11){
      this.year = this.year + 1;
      this.month = 0;
      this.getDayNames();
    }else {
      this.month++;
      this.getDayNames();
    }
  }

  prev():void{
    if (this.month === 0){
      this.year = this.year - 1;
      this.month = 11;
      this.getDayNames();
    }else {
      this.month--;
      this.getDayNames();
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.pageWidth  = event.target.innerWidth;

  }


  getDayss(): Array<string>{
    let days = [];
    if (this.pageWidth > 1024){
      days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      return days;
    }else {
      days = ['Sun', 'Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
      return days;
    }
  }

  getToday(day: any): boolean{
    let status;

    const datess = new Date();
    const today = String(datess.getDate());
    // tslint:disable-next-line:radix
    if (day){
      status = parseInt(today) === parseInt(day.toString()) && this.month === datess.getMonth() && this.year === datess.getFullYear();

      return status;
    }

  }


  closeNav(): void{
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("overlay").style.display = "none";

  }

  openNav(): void {
    document.getElementById("mySidenav").style.width = "280px";
    document.getElementById("overlay").style.display = "block";


  }



}
