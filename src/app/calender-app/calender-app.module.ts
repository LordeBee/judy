import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalenderAppRoutingModule } from './calender-app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [DashboardComponent],
    imports: [
        CommonModule,
        CalenderAppRoutingModule,
        SharedModule
    ]
})
export class CalenderAppModule { }
