import {Component, HostListener, OnInit} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import {Router} from '@angular/router';
import {StorageServiceService} from '../../../services/storage-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {


  pageWidth: any;
  userData: any;
  email = '';
  initials = '';
  role = '';

  constructor(
    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private router: Router, public storageService: StorageServiceService) {

    this.pageWidth = window.innerWidth;

    iconRegistry.addSvgIcon(
      'user',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/user.svg'));

    iconRegistry.addSvgIcon(
      'apps',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/apps.svg'));

    iconRegistry.addSvgIcon(
      'notifications',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/notifications.svg'));

    iconRegistry.addSvgIcon(
      'search',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/search.svg'));

    iconRegistry.addSvgIcon(
      'logout',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/logout.svg'));


  }

  ngOnInit(): void {
    this.pageWidth = window.innerWidth;
    this.userData =  JSON.parse(this.storageService.getData());
    this.email = this.userData.profileInfo.email;
    this.initials = this.getFirstLetter(this.userData.profileInfo.fname) + this.getFirstLetter(this.userData.profileInfo.sname);
    console.log(this.userData)
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.pageWidth  = event.target.innerWidth;
  }

  userProfile(): void{
    this.router.navigate(['/dashboard/profile']);
  }

  logout(): void{
    this.router.navigate(['/login']);
  }

  getFirstLetter(name: any): string{
    if (name){
      return name.substring(0, 1);
    }else{
      return 'U';
    }
  }

}
