import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {hostHeaderInfo} from '../models/hostHeaderInfo';
import {IpServiceService} from '../../services/ip-service.service';
import {AccountsServicesService} from '../../services/accounts-services.service';
import {StorageServiceService} from '../../services/storage-service.service';
import {ToastService} from '../../services/toast.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  welcomeSubText = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.';

  loginFormGroup: FormGroup;
  authResp: any;
  loader = false;
  formBody: any;
  public hostHeaderInfo: hostHeaderInfo;

  constructor(private router: Router, private formBuilder: FormBuilder, public ipService: IpServiceService, public apiService: AccountsServicesService, public storageService: StorageServiceService, private toastService: ToastService) {

    this.hostHeaderInfo = new hostHeaderInfo();

    if (!this.formBody){
      this.formBody = {
        hostHeaderInfo: '',
        email: '',
        password: '',
        deviceTypeID: 1,
        ipAddress: '',
        deviceID: ''
      };
    }

    this.loginFormGroup = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.getIP();
    this.formBody.hostHeaderInfo = this.hostHeaderInfo;

  }

  forgot(): void{
    this.router.navigate(['/forgot-password']);
  }

  login(): void{
   this.formBody.email =  this.loginFormGroup.get('email').value;
   this.formBody.password =  this.loginFormGroup.get('password').value;
   this.loader = true;
   this.apiService.authenticateUser(JSON.stringify(this.formBody)).subscribe( res => {
      this.authResp = res;
      if (this.authResp.hostHeaderInfo.responseCode === '000'){
        this.storageService.saveToken(this.authResp.hostHeaderInfo.token);
        this.storageService.saveUserData(JSON.stringify(this.authResp));
        this.router.navigate(['/dashboard/dashboard']);
      }else{
        this.loader = false;
        this.showToast(this.authResp.hostHeaderInfo.responseMessage);
      }
    }, error => {
     this.loader = false;
     this.showToast('Please try again later');

   });
  }


  getIP(): void
  {
    this.ipService.getIPAddress().subscribe((res: any) => {
      this.formBody.ipAddress = res.ip;
    });
  }

  showToast(message: any): void {
    this.toastService.show({
      text: message,
      type: 'success',
    });
  }

}
