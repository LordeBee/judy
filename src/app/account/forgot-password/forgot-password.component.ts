import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AccountsServicesService} from '../../services/accounts-services.service';
import {StorageServiceService} from '../../services/storage-service.service';
import {ToastService} from '../../services/toast.service';
import {IpServiceService} from '../../services/ip-service.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  loginFormGroup: FormGroup;
  authResp: any;
  loader = false;

  welcomeSubText = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.';


  constructor(private router: Router, private formBuilder: FormBuilder, public ipService: IpServiceService, public apiService: AccountsServicesService, public storageService: StorageServiceService, private toastService: ToastService) { }

  ngOnInit(): void {

    this.loginFormGroup = this.formBuilder.group({
      email: ['', Validators.required]
    });
  }

  reset(): void{
    this.loader = true;
    this.apiService.resetPassword(this.loginFormGroup.get('email').value).subscribe( res => {
      this.authResp = res;
      if (this.authResp.hostHeaderInfo.responseCode === '000'){
        this.router.navigate(['/reset-password']);
      }else{
        this.loader = false;
        this.showToast(this.authResp.hostHeaderInfo.responseMessage);
      }
    }, error => {
      this.loader = false;
      this.showToast('Please try again later');

    });

  }


  showToast(message: any): void {
    this.toastService.show({
      text: message,
      type: 'success',
    });
  }


}
