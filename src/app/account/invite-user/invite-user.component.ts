import { Component, OnInit } from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {hostHeaderInfo} from '../models/hostHeaderInfo';
import {MyErrorStateMatcher} from '../reset-password/MyErrorStateMatcher';
import {AccountsServicesService} from '../../services/accounts-services.service';
import {StorageServiceService} from '../../services/storage-service.service';
import {ToastService} from '../../services/toast.service';

@Component({
  selector: 'app-invite-user',
  templateUrl: './invite-user.component.html',
  styleUrls: ['./invite-user.component.scss']
})
export class InviteUserComponent implements OnInit {

  avatar: any = '/assets/imgs/profile-holder.png';
  hide: boolean = true;
  hideRe: boolean = true;
  resp: any;
  loginFormGroup: FormGroup;
  authResp: any;
  loader = false;
  formBody: any;
  public hostHeaderInfo: hostHeaderInfo;
  matcher = new MyErrorStateMatcher();
  isData = false;


  constructor(iconRegistry: MatIconRegistry,
              public sanitizer: DomSanitizer,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              public apiService: AccountsServicesService,
              public storageService: StorageServiceService,
              private toastService: ToastService) {

    iconRegistry.addSvgIcon(
      'eye-off',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/eye-off.svg'));

    iconRegistry.addSvgIcon(
      'invite-done',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/invite-done.svg'));

    this.hostHeaderInfo = new hostHeaderInfo();

    if (!this.formBody){
      this.formBody = {
        hostHeaderInfo: '',
        id: '',
        address: '',
        address2: '',
        city: '',
        state: '',
        zip: '',
        password: '',
        confirmPassword: ''
      };
    }

    this.loginFormGroup = this.formBuilder.group({
      name: ['' , Validators.required],
      email: ['' , Validators.required],
      phone: ['' , Validators.required],
      id: ['' , Validators.required],
      address: ['' , Validators.required],
      address2: ['' , Validators.required],
      city: ['' , Validators.required],
      state: ['' , Validators.required],
      zip: ['' , Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['']
    }, {validator: this.checkPasswords });
  }

  ngOnInit(): void {

    document.getElementById('avatar').style.backgroundImage = `url(${this.avatar})`;
    // this.formBody.hostHeaderInfo = this.hostHeaderInfo;
    // this.route.params.subscribe(params => {
    //   this.apiService.getUserResetInfo(params['uuid']).subscribe( res => {
    //     this.resp = res;
    //     if(this.resp.hostHeaderInfo.responseCode === '000'){
    //       this.isData = true;
    //     }else{
    //       this.router.navigate(['/login']);
    //     }
    //   },error => {
    //     this.router.navigate(['/login']);
    //   });
    // });
  }

  passwordToggle(): void {
    this.hide = !this.hide;
  }

  passwordToggleRe(): void {
    this.hideRe = !this.hideRe
    ;
  }


  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true };
  }

  showToast(message: any): void {
    this.toastService.show({
      text: message,
      type: 'success',
    });
  }

  submit(): void{

    this.formBody.address =  this.loginFormGroup.get('address').value;
    this.formBody.address2 =  this.loginFormGroup.get('address2').value;
    this.formBody.city =  this.loginFormGroup.get('city').value;
    this.formBody.state =  this.loginFormGroup.get('state').value;
    this.formBody.zip =  this.loginFormGroup.get('zip').value;
    this.formBody.password =  this.loginFormGroup.get('password').value;
    this.formBody.confirmPassword =  this.loginFormGroup.get('confirmPassword').value;
    this.loader = true;

  }


  getFile(event): void {

    this.handleInputChange(event.target.files[0]);

  }

  handleInputChange(files): any {
    const file = files;
    const pattern = /image-*/;
    const reader = new FileReader();

    if (!file.type.match(pattern)) {
      this.showToast('invalid format');
      return;
    }
    else if (file.size > 1000000){
      this.showToast('File size 1MB exceeded');
      return;
    }

    else{
      reader.onloadend = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
    }

  }

  _handleReaderLoaded(e): any {
    const reader = e.target;
    this.imageConvert(reader.result.substr(reader.result.indexOf(',') + 1));
    console.log(reader.result.substr(reader.result.indexOf(',') + 1));
  }


  imageConvert(logo): any {
    if (logo) {
      console.log(logo)
      this.avatar =  this.sanitizer.bypassSecurityTrustUrl('data:Image/*;base64,' + logo);
      var img = "url('data:image/png;base64, "+logo + "')";
      document.getElementById('avatar').style.backgroundImage = img;

    }else {
      this.avatar = 'assets/imgs/profile-holder.png';

    }
  }

  getBg(): any{
    return this.avatar;
  }



}
