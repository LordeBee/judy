import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {hostHeaderInfo} from '../models/hostHeaderInfo';
import {IpServiceService} from '../../services/ip-service.service';
import {AccountsServicesService} from '../../services/accounts-services.service';
import {StorageServiceService} from '../../services/storage-service.service';
import {ToastService} from '../../services/toast.service';
import {MyErrorStateMatcher} from './MyErrorStateMatcher';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  welcomeSubText = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.';


  loginFormGroup: FormGroup;
  authResp: any;
  loader = false;
  formBody: any;
  public hostHeaderInfo: hostHeaderInfo;
  matcher = new MyErrorStateMatcher();

  constructor(private router: Router, private formBuilder: FormBuilder, public ipService: IpServiceService, public apiService: AccountsServicesService, public storageService: StorageServiceService, private toastService: ToastService) {

    this.hostHeaderInfo = new hostHeaderInfo();

    if (!this.formBody){
      this.formBody = {
        hostHeaderInfo: '',
        userID: 1,
        password: ''
      };
    }

    this.loginFormGroup = this.formBuilder.group({
      password: ['', Validators.required],
      confirmPassword: ['']
    }, {validator: this.checkPasswords });
  }

  ngOnInit(): void {
    this.formBody.hostHeaderInfo = this.hostHeaderInfo;
  }

  login(): void{
    this.formBody.password =  this.loginFormGroup.get('password').value;
    this.loader = true;
    this.apiService.changePassword(JSON.stringify(this.formBody)).subscribe( res => {
      this.authResp = res;
      if (this.authResp.hostHeaderInfo.responseCode === '000'){
        this.showToast('Password changed successfully, Login');
        this.router.navigate(['/login']);
      }else{
        this.loader = false;
        this.showToast(this.authResp.hostHeaderInfo.responseMessage);
      }
    }, error => {
      this.loader = false;
      this.showToast('Please try again later');

    });

  }


  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true };
  }

  showToast(message: any): void {
    this.toastService.show({
      text: message,
      type: 'success',
    });
  }



}
