import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from './account/account.component';
import { LoginComponent } from './login/login.component';
import {FlexModule} from '@angular/flex-layout';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { InviteUserComponent } from './invite-user/invite-user.component';
import {SharedModule} from '../shared/shared.module';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  declarations: [AccountComponent, LoginComponent, ForgotPasswordComponent, ResetPasswordComponent, InviteUserComponent],
    imports: [
        CommonModule,
        AccountRoutingModule,
        FlexModule,
        MatFormFieldModule,
        MatInputModule,
        SharedModule,
        HttpClientModule
    ]
})
export class AccountModule { }
