import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {Router} from '@angular/router';
import * as Highcharts from 'highcharts';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {SingleDataSet, Label, PluginServiceGlobalRegistrationAndOptions, Color} from 'ng2-charts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  highcharts = Highcharts;

  options = {
    responsive: true,
    cutoutPercentage: 80,
    elements: {
      arc: {
        borderWidth: 0
      }
    }
  };

  public doughnutChartColors: any[] = [{
    backgroundColor: ['#DCC5CA', '#883B4C', '#B58590']
  }];

  public doughnutChartLabels = ['Download Sales', 'In-Store Sales'];
  public doughnutChartData: any = [
    [350, 450, 200]
  ];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartPlugins: any[] = [{
    afterDraw(chart) {
      const ctx = chart.ctx;
      var txt1 = 'Total Users';
      var txt2 = '293';


      const sidePadding = 100;
      const sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)

      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      const centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
      const centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);


      const stringWidth = ctx.measureText(txt1).width;
      const elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

      // Find out how much the font can grow in width.
      const widthRatio = elementWidth / stringWidth;
      const newFontSize = Math.floor(30 * widthRatio);
      const elementHeight = (chart.innerRadius * 2);

      // Pick a new font size so it will not be larger than the height of label.
      const fontSizeToUse = 20;
      ctx.font = fontSizeToUse + 'px Muli-Regular';
      ctx.fillStyle = '#fff';

      // Draw text in center
      ctx.fillText(txt2, centerX, centerY - 10);
      var fontSizeToUse1 = 15;
      ctx.font = fontSizeToUse1 + 'px Muli-Regular';
      ctx.fillText(txt1, centerX, centerY + 10);
    }
  }];



  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    animation: false,
    scaledisplay:false,
    responsive: true,

    scales: {
      xAxes: [
        {
          display: false
        }
      ],
      yAxes: [
        {
          display: true
        }
      ]
    }

  };
  public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];

  public barChartColors: Color[] = [
    { backgroundColor: '#6C0E23' },
  ];

  layoutsFirstLeft = '40';
  layoutsFirstRight = '60';
  pageWidth: any;
  layouts = 'row';
  gap = '50px';

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private router: Router) {


    iconRegistry.addSvgIcon(
      'search-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/search-profile.svg'));


    iconRegistry.addSvgIcon(
      'mine-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mine-profile.svg'));

    iconRegistry.addSvgIcon(
      'admin-dashboard',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/admin-dashboard.svg'));

    iconRegistry.addSvgIcon(
      'clients-dashboard',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/clients-dashboard.svg'));

    iconRegistry.addSvgIcon(
      'pending-dashboard',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/pending-dashboard.svg'));


    iconRegistry.addSvgIcon(
      'activity-blue',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/activity-blue.svg'));

    iconRegistry.addSvgIcon(
      'activity-green',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/activity-green.svg'));

    iconRegistry.addSvgIcon(
      'activity-red',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/activity-red.svg'));

    iconRegistry.addSvgIcon(
      'activity-time',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/activity-time.svg'));
  }

  ngOnInit(): void {

    this.pageWidth = window.innerWidth;
    if(this.pageWidth < 767){
      this.layoutsFirstLeft = '100';
      this.layoutsFirstRight = '100';
      this.layouts = 'column';
      this.gap = '10px';

    }else{
      this.layoutsFirstLeft = '40';
      this.layoutsFirstRight = '60';
      this.layouts = 'row';
      this.gap = '50px';
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.pageWidth = event.target.innerWidth;
    if(this.pageWidth < 767){
      this.layoutsFirstLeft = '100';
      this.layoutsFirstRight = '100';
      this.layouts = 'column';
      this.gap = '10px';

    }else{
      this.layoutsFirstLeft = '40';
      this.layoutsFirstRight = '60';
      this.layouts = 'row';
      this.gap = '50px';
    }
  }

  openNav(): void {
  }

  goToApp(route: string): void{
    this.router.navigate([route]);
  }

}
