import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppsComponent} from './apps/apps.component';
import {ProfileComponent} from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {path: 'apps', component: AppsComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'dashboard', component: DashboardComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
