import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { AppsComponent } from './apps/apps.component';
import {FlexModule} from '@angular/flex-layout';
import { ProfileComponent } from './profile/profile.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {SharedModule} from '../shared/shared.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [AppsComponent, ProfileComponent, DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FlexModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule,
    HighchartsChartModule,
    ChartsModule
  ]
})
export class DashboardModule { }
