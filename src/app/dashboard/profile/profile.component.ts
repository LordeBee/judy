import { Component, OnInit } from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  avatar = 'assets/imgs/avatar.png';
  hide: boolean = true;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {

    iconRegistry.addSvgIcon(
      'eye-off',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/eye-off.svg'));
  }

  ngOnInit(): void {
  }

  passwordToggle(): void {
    this.hide = !this.hide;
  }

}
