import {Component, Inject, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


export interface NewEngageDialogData {
  name: string;
  role: string;


}
@Component({
  selector: 'app-new-engagement-dialog',
  templateUrl: './new-engagement-dialog.component.html',
  styleUrls: ['./new-engagement-dialog.component.scss']
})
export class NewEngagementDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<NewEngagementDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: NewEngageDialogData) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
