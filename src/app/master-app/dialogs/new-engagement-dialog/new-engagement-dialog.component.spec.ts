import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEngagementDialogComponent } from './new-engagement-dialog.component';

describe('NewEngagementDialogComponent', () => {
  let component: NewEngagementDialogComponent;
  let fixture: ComponentFixture<NewEngagementDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewEngagementDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEngagementDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
