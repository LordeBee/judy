import {Component, Inject, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
declare var $:any;



export interface UploadDialogData {
  name: string;
  role: string;


}

@Component({
  selector: 'app-upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['./upload-dialog.component.scss']
})
export class UploadDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<UploadDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: UploadDialogData) { }

  ngOnInit(): void {

    setTimeout(() => {
      $('.dropify').dropify({
        tpl: {
          wrap:            '<div class="dropify-wrapper" style="width: 95%; border-radius: 5px; background: #D9D9D9; border: none;"></div>',
          message:         '<div class="dropify-message">' +
            '<img src="/assets/icons/upload.png"> ' +
            '<p style="font-family: Muli-Bold; color: #35475D; font-size: 22px">Upload Document</p>' +
            '<p style="color: #3B3B3B; font-family: Muli-Light; font-size: 16px;">Drag and drop to upload or </p>' +
            '<p style="color: #3B3B3B; font-family: Muli-Light; font-size: 16px;">click here to upload</p>' +
            '</div>'
        }
      });
    }, 10);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
