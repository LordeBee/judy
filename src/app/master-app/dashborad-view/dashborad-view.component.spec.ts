import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboradViewComponent } from './dashborad-view.component';

describe('DashboradViewComponent', () => {
  let component: DashboradViewComponent;
  let fixture: ComponentFixture<DashboradViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboradViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboradViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
