import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {NewEngagementDialogComponent} from '../dialogs/new-engagement-dialog/new-engagement-dialog.component';
import {ExpenseDialogComponent} from '../dialogs/expense-dialog/expense-dialog.component';
import {UploadDialogComponent} from '../dialogs/upload-dialog/upload-dialog.component';

@Component({
  selector: 'app-dashborad-view',
  templateUrl: './dashborad-view.component.html',
  styleUrls: ['./dashborad-view.component.scss']
})
export class DashboradViewComponent implements OnInit {


  filter: any;
  activity = '';
  data = [
    { name: 'Hannah Christine Baers' , initials: 'BA', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    { name: 'Hannah Christine Baers' , initials: 'BA', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    { name: 'Hannah Christine Baers' , initials: 'BA', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    { name: 'Hannah Christine Baers' , initials: 'BA', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    { name: 'Hannah Christine Baers' , initials: 'BA', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    { name: 'Hannah Christine Baers' , initials: 'BA', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    { name: 'Hannah Christine Baers' , initials: 'BA', desc: 'Lorem ipsum dolor sit amet, consectetuer '},
    { name: 'Hannah Christine Baers' , initials: 'BA', desc: 'Lorem ipsum dolor sit amet, consectetuer '}
  ];
  selectedIndex = 0;
  selectedOption = 0;
  sampleText = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.';
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, public dialog: MatDialog, private router: Router) {

    iconRegistry.addSvgIcon(
      'mine-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mine-profile.svg'));

    iconRegistry.addSvgIcon(
      'search-profile',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/search-profile.svg'));


    iconRegistry.addSvgIcon(
      'data-master',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/data-master.svg'));

    iconRegistry.addSvgIcon(
      'invoice-master',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/invoice-master.svg'));

    iconRegistry.addSvgIcon(
      'new-master',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/new-master.svg'));
    this.filter = '';
  }

  ngOnInit(): void {


  }

  add(): void{
    this.dialog.open(NewEngagementDialogComponent,{
      height: '750px',
      width: '500px',
    });
  }

  view(): void{
    this.dialog.open(ExpenseDialogComponent,{
      height: '800px',
      width: '800px',
    });
  }

  upload(): void{
    this.dialog.open(UploadDialogComponent,{
      height: '400px',
      width: '550px',
    });
  }



}
