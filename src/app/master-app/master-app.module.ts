import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterAppRoutingModule } from './master-app-routing.module';
import { DashboradViewComponent } from './dashborad-view/dashborad-view.component';
import {SharedModule} from '../shared/shared.module';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import { NewEngagementDialogComponent } from './dialogs/new-engagement-dialog/new-engagement-dialog.component';
import { UploadDialogComponent } from './dialogs/upload-dialog/upload-dialog.component';
import { ExpenseDialogComponent } from './dialogs/expense-dialog/expense-dialog.component';


@NgModule({
  declarations: [DashboradViewComponent, NewEngagementDialogComponent, UploadDialogComponent, ExpenseDialogComponent],
  entryComponents: [NewEngagementDialogComponent, UploadDialogComponent, ExpenseDialogComponent],
    imports: [
        CommonModule,
        MasterAppRoutingModule,
        SharedModule,
      Ng2SearchPipeModule
    ]
})
export class MasterAppModule { }
