import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboradViewComponent} from './dashborad-view/dashborad-view.component';

const routes: Routes = [
  {path: 'dashboard', component: DashboradViewComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterAppRoutingModule { }
